# SuperTuxParty
![SuperTuxParty Logo](assets/icons/icon-smallest.png)

A [free/libre](https://www.gnu.org/philosophy/free-sw.html) and
[open-source](https://opensource.org/docs/osd/) party game that is meant to
replicate the feel of games such as Mario Party.

![Mini-game Screenshot](screenshot.png)

## Engine
SuperTuxParty is built with the [Godot Engine](https://godotengine.org/).
The latest stable version, 3.0.6, is used.

## Issues
If you have ideas for mini-games, design improvements or have found a bug then
please report that under Issues.

## License
All code is licensed under the [GNU GPL V3.0](https://www.gnu.org/licenses/gpl.html),
see the **LICENSE** file for more information.

All other data such as art, sound, music, and etc. is usually
released under the [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
though it should always be specified which license it falls under,
see the **LICENSE-ART** file for details.

## Community
We also have a [subreddit](https://www.reddit.com/r/SuperTuxParty/)
for discussions about the project, and two Matrix servers for
[development](https://matrix.to/#/#SuperTuxParty-Dev:matrix.org) and
[general](https://matrix.to/#/#SuperTuxParty-Extra:matrix.org) talking.
