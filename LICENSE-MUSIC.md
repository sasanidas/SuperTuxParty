## assets/music/
### main menu.ogg

Public Domain by Rezoner

Modified by RiderExMachina (David Seward)

Retrieved from [Open Game Art](https://opengameart.org/content/happy-arcade-tune)

License [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)